# simdal: simulating message propagation on random networks

# Usage

- Simulation parameters are currently hardcoded, partially in `gen.sh` and partially in
  the scenario encoded in `bin/main.ml`
- `./gen.sh --analyse bandwidth --analyse shards --analyse consumer`

# Repo structure

- `lib/` contains the network sampler and (eager) message propagation model
- `bin/` contains the main scenario for the DAL and the various analyses:
  - attester failure probability
  - consumer failure probability
  - bandwidth requirements

# TODOs

- [ ] sim: cleaner way to parameterize the simulation (e.g. json-based)
- [ ] analysis: compute success probability of slot (at least 50% of all attestors succeeded)
- [ ] sim: strategy for --min-relay is perhaps too aggressive for small attestors
- [ ] sim: lazy/mixed message propagation
- [ ] sim: propagation time analysis
- [ ] stake: more realistic committee computation

# Methodology

## tl;dr

We estimate bandwidth requirements and topological properties of the P2P graph of the DAL network. Current repo: https://gitlab.com/igarnier/simdal

## Parameters

The simulation has the following parameters:
- size of the graph $N$,
- degree constraints on each node $target : \{0;\ldots;N-1\} \rightarrow \mathbb N \times \mathbb N$ (representing an inclusive interval on the desired number of neighbours for each node),
- topic set for each node $\mathcal T_i$.

We define the maximal graph $\mathcal M$ on $N$ nodes as the undirected graph containing all edges $(i, j)$ such that $T_i \cap T_j \neq \emptyset$.

## Modelling assumptions

The following sums up what the simulator implements. The simulator is not concerned with the dynamics of the network.

- The DAL parameters are set to $2048$ shards, $256$ slots
- A topic is modelled as a pair $slot, shard$
- We assume that the p2p graph is at steady state. Our model of the p2p graph at steady state is that it is sampled uniformly among all subgraphs of $\mathcal M$ that minimize an error function encoding the degree constraints.
- We only simulate eager message propagation for one slot (slot $0$). Multiply the relevant bandwidth estimates by the actual number of slots in the target design.
- Messages are propagated around a routing sampled uniformly at random.

## Network sampling methodology

We construct a Markov chain over graphs with $N$ vertices. The Markov chain describes a sequence of graphs $\mathcal G_t$ with $\mathcal G_0$ the totally disconnected graph on $N$ vertices. The transition kernel from $G_t$ is a simple edge-flipping process:
- pick an edge $e$ uniformly at random in $\mathcal M$
- if $e \not \in \mathcal G_t$ then $\mathcal G_{t+1} = \mathcal G_t \cup e$, otherwise $\mathcal G_{t+1} = \mathcal G_t \setminus e$.

This dynamics respects the topic set constraints but not the degree constraints. In order to steer it towards graphs respecting both sets of constraints, we adjoin to this transition kernel an energy function $\mathcal E$ and construct a [Metropolis-Hastings](https://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm) Markov chain that samples low-energy (i.e. low error) graphs.

Given a graph $\mathcal G$ and a node $x \in \{0;\ldots;N-1\}$, we define the error at $x$ as:
$$
\mathcal E_x = \inf_{t\in target(x)} |deg(x) - t|
$$

The global error function on $\mathcal G$ is
$$
\mathcal E = (\sum_x \mathcal E_x) ^ 2
$$

Hence the error is $0$ on a graph respecting all constraints.

Each graph is sampled from the Metropolis-Hasting Markov chain with the edge-flipping transition kernel and energy function $\mathcal E$. In order to decrease autocorrelation between samples, we drop a large (O(number of edges)) number of samples between each graph.

This yields a model of the topology a steady state.

## Bandwidth estimation methodology

We want to estimate bandwidth requirements on a given graph $\mathcal G$. For each shard, the slot producer broadcasts a message on the subnetwork corresponding to that shard. Assuming we simulate this propagation, bandwidth requirements can be estimated by accumulating statistics on the number of input (received) and output (sent) messages for each node over all shards. Of course, propagation is not deterministic and we need a model for it as well. Pseudo-code for the bandwidth estimation loop would look as follows:

```
for many random graphs G:

  for s = 0 to 2047:

     let G_s = connected subgraph of G on topic s including producer

     for many simulated propagations of a message from producer on G_s

       for each node in G_s
         count numbers of sent and received messages
       done

     done

  done

  record total in/out statistics for each node

done

normalize stats by number of random graphs x number of propagations per graph
```

We must therefore design a model for eager message propagation. Eager message propagation here is a synonym for 'push-based' rather than 'pull-based' propagation. Intuitively, we assume that upon receiveing a new message, a node $x$ will spread it as fast as it can to its neighbours $y_i$ (except the node through which the new message was received of course). The only case where the node $x$ does not send a message to a particular neighbour $y_i$ is if that neighbour also sent $x$ the new message beforehand. To sum up, $x$ sends a message to all its neighbours except those nodes it **knows** already have it. This highlights that eager propagation makes it possible to receive a message several times. It also highlights that for any edge $(x, y)$ in the shard subnetwork with eager propagation, either $x$ sends the message to $y$ or the other way around.

Simulating eager message propagation thus amounts to sampling an orientation of the edges of the subgraph, such that there exists a directed path from the producer to each node of the subgraph.

Lazy propagation on the other hand corresponds to each node receiving the shard message only once (this explicitly does not model communication of metadata messages such as `IHAVE/IWANT` in gossipsub). Lazy propagation on a subgraph from a designated producer can be modelled as following the edges of a spanning tree.

Our model of propagation is therefore the following:
1. sample a uniform spanning tree rooted at the producer,
2. (only for eager propagation) sample an orientation uniformly at random for the remaining edges.

We use the Aldous-Broder algorithm for [uniformly sampling spanning trees](https://en.wikipedia.org/wiki/Loop-erased_random_walk).

This defines a model for message propagation.


## Possible improvements

- Uniform spanning trees are perhaps not the most realistic in terms of message propagation. Assuming propagation speed is the same over the whole network, one expects all the branches of the tree to have roughly the same length. It is possible to perform importance sampling to couple the uniform spanning tree sampler with a density making 'balanced' spanning trees more likely.

- We could probably obtain statistics on global propagation time if provided with a model of per-edge propagation time and per-node processing time
