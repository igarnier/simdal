module Tree = Simdal.Sampler.Network_stats_helpers.Tree

type tree = Tree.t

type subgraph_predicate = Simdal.G.vertex -> bool

type 'a t =
  { pre_shard_iteration :
      Cmdline.options -> Roles.cfg -> Simdal.Sampler.state -> unit;
    sample :
      Cmdline.options ->
      Roles.cfg ->
      Simdal.Sampler.state ->
      shard:int ->
      subgraph_predicate ->
      tree list ->
      unit;
    post_shard_iteration :
      Cmdline.options -> Roles.cfg -> Simdal.Sampler.state -> unit;
    post_simulation : Cmdline.options -> Roles.cfg -> 'a
  }

let void =
  { pre_shard_iteration = (fun _ _ _ -> ());
    sample = (fun _ _ _ ~shard:_ _ _ -> ());
    post_shard_iteration = (fun _ _ _ -> ());
    post_simulation = (fun _ _ -> ())
  }

let postcompose f a =
  { a with post_simulation = (fun cfg opts -> f (a.post_simulation cfg opts)) }

let proportion_of_attesters_in_neighbourhood id =
  let atts = ref 0 in
  let total = ref 0 in
  { void with
    pre_shard_iteration =
      (fun _opts cfg state ->
        Simdal.G.iter_pred
          (fun v ->
            incr total ;
            if Roles.is_attester cfg v then incr atts else ())
          (Simdal.Sampler.graph state)
          id)
  }

let receives_enough_shards ~receiver ~target_shard_count =
  let not_enough_shards = ref 0 in
  let fill_rate = ref 0.0 in
  let shards_received = ref 0 in
  { pre_shard_iteration = (fun _opts _cfg _state -> shards_received := 0);
    sample =
      (fun _opts _cfg _state ~shard:_ _subgraph trees ->
        match trees with
        | [] -> failwith "Analysis.receives_enough_shards, sample"
        | tree :: _ ->
            (* All spanning trees are equivalent from the POV of shard reception,
               we only look at the first one *)
            if Tree.mem_vertex tree receiver then incr shards_received else ());
    post_shard_iteration =
      (fun _opts _cfg _state ->
        let shards_received = float !shards_received in
        fill_rate := !fill_rate +. shards_received ;
        if shards_received < target_shard_count then incr not_enough_shards);
    post_simulation =
      (fun opts _cfg ->
        (* probability that the receiver fails *)
        let att_fail = float !not_enough_shards /. float opts.Cmdline.ngraphs in
        (* average shard fill rate for largest receiver *)
        let att_rate =
          !fill_rate /. (float opts.Cmdline.ngraphs *. target_shard_count)
        in
        (att_fail, att_rate))
  }

let estimate_bandwidth rng_state () =
  let open Simdal in
  let samples = ref [] in
  let bwth_stats = ref (Sampler.create_bandwidth_stats ()) in
  { pre_shard_iteration =
      (fun _opts _cfg _state -> bwth_stats := Sampler.create_bandwidth_stats ());
    sample =
      (fun _opts _cfg state ~shard:_ subgraph_predicate trees ->
        Sampler.estimate_bandwidth
          ~state
          ~subgraph_predicate
          ~counters:!bwth_stats
          ~spanning_trees:trees
          rng_state);
    post_shard_iteration =
      (fun _opts _cfg _state -> samples := !bwth_stats :: !samples);
    post_simulation =
      (fun opts cfg ->
        let module T = G.Vertex_table in
        let nsamples = List.length !samples in
        assert (opts.ngraphs = nsamples) ;
        let vcount = Roles.count cfg in
        let in_table = Array.make_matrix vcount nsamples 0.0 in
        let out_table = Array.make_matrix vcount nsamples 0.0 in
        List.iteri
          (fun sample_index { Sampler.incoming; outgoing } ->
            T.iter
              (fun v count -> in_table.(v).(sample_index) <- !count)
              incoming ;
            T.iter
              (fun v count -> out_table.(v).(sample_index) <- !count)
              outgoing)
          !samples ;
        let stats table =
          Array.to_seq table
          |> Seq.map (fun samples ->
                 let mean = Stats.Emp.Float.empirical_mean samples in
                 let std = sqrt (Stats.Emp.Float.empirical_variance samples) in
                 let min = Array.fold_left Float.min Float.max_float samples in
                 let max = Array.fold_left Float.max Float.min_float samples in
                 (mean, std, min, max))
          |> Array.of_seq
        in
        (stats in_table, stats out_table))
  }
