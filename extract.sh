#!/bin/bash

relayed=( 0 5 10 15 25 50 75 100 125 )

rm -f results.dat

for P in `seq 20 20 140`; do
    for r in "${relayed[@]}"; do
        FILE=result-$P-$r.txt
        ATT0_FAILS=$(sed -n 20p $FILE | awk '{ print $2 }')
        ATT0_RATIO=$(sed -n 20p $FILE | awk '{ print $4 }')
        ATT1_FAILS=$(sed -n 23p $FILE | awk '{ print $2 }')
        ATT1_RATIO=$(sed -n 23p $FILE | awk '{ print $4 }')
        ATT2_FAILS=$(sed -n 26p $FILE | awk '{ print $2 }')
        ATT2_RATIO=$(sed -n 26p $FILE | awk '{ print $4 }')
        # CONS_FAILS=$(sed -n 18p $FILE | awk '{ print $6 }')
        # CONS_RATIO=$(sed -n 18p $FILE | awk '{ print $8 }')
        # ATT_IN=$(sed -n 24p $FILE | awk '{ print $10 }')
        # ATT_OUT=$(sed -n 25p $FILE | awk '{ print $10 }')
        echo "$P $r $ATT0_FAILS $ATT0_RATIO" >> attester_0.dat
        echo "$P $r $ATT1_FAILS $ATT1_RATIO" >> attester_1.dat
        echo "$P $r $ATT2_FAILS $ATT2_RATIO" >> attester_2.dat
    done
done
