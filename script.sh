#!/bin/bash

ROUTINGS=30
GRAPHS=300

relayed=( 0 5 10 15 25 50 75 100 125 )

EXE=_build/default/bin/main.exe

## Avg degree of slot consumers. We set it to 40.
C=40
LC=$( expr $C - 10 )
UC=$( expr $C + 10 )

NATT=129
NPRD=256
NCONS=256

## Extra connections for the attester, on top of what is required by the consumers and the producers
SLACK=20

rm -f commands.txt

EXTRA_OPTS="${@:1}"

DIR=results_$(date "+%F-%Hh%M.%S")

mkdir -p $DIR

for P in `seq 20 20 140`; do
    LP=$( expr $P - 5 )
    UP=$( expr $P + 5 )
    A=$( expr  \( $NPRD \* $P + $NCONS \* $C + $NATT \* $SLACK \) / $NATT )
    LA=$( expr $A - 5 )
    UA=$( expr $A + 5 )
    echo "Launching simulations for producer degree = $P"
    for r in "${relayed[@]}"; do
        FILE=$DIR/result-$P-$r.txt
        echo $EXE --min-relayed $r --ngraphs $GRAPHS --nroutings $ROUTINGS --prod-deg $LP $UP --att-deg $LA $UA --cons-deg $LC $UC --to-file $FILE $EXTRA_OPTS >> commands.txt
    done
    wait
done

parallel < commands.txt
