open Simdal

(*
  Testing the error function on node degrees
*)

let%expect_test "node_error" =
  for i = 0 to 30 do
    Printf.printf "%d => %f " i (Sampler.node_error (5, 10) i)
  done ;
  [%expect
    {| 0 => 5.000000 1 => 4.000000 2 => 3.000000 3 => 2.000000 4 => 1.000000 5 => 0.000000 6 => 0.000000 7 => 0.000000 8 => 0.000000 9 => 0.000000 10 => 0.000000 11 => 1.000000 12 => 2.000000 13 => 3.000000 14 => 4.000000 15 => 5.000000 16 => 6.000000 17 => 7.000000 18 => 8.000000 19 => 9.000000 20 => 10.000000 21 => 11.000000 22 => 12.000000 23 => 13.000000 24 => 14.000000 25 => 15.000000 26 => 16.000000 27 => 17.000000 28 => 18.000000 29 => 19.000000 30 => 20.000000 |}]

(*
  Testing sampler initialization
*)

module Topic = struct
  type t = int

  let pp = Format.pp_print_int

  let equal = Int.equal

  let compare = Int.compare

  let hash = Hashtbl.hash
end

module Topic_set = Set.Make (Topic)

let state =
  let all_slots =
    Array.to_seq (Array.init 256 (fun i -> i)) |> Topic_set.of_seq
  in
  Sampler.create_empty
    400
    ~kind:(fun _ -> all_slots)
    ~compat:(fun set1 set2 -> not (Topic_set.disjoint set1 set2))
    ~bounds:(fun _ -> (4, 9))

let t1 = Unix.gettimeofday ()

let rng_state = Random.State.make_self_init ()

let after_burn_in =
  Sampler.network ~verbosity:`Silent ~initial:state ~burn_in:100_000 rng_state

let _seq = Seq.of_dispenser (fun () -> Some (after_burn_in rng_state))
